var rn = require('random-number');

var gen = rn.generator({
    min:  0
  , max:  4
  , integer: true
  })

var nota = rn.generator({
    min:  5
  , max:  10
  , integer: true
  })

let generatorService = {

    getReview: async function getReview() {

    let cor = ['ambar','amarelo claro','de tom negro','avermelhada','alaranjada']
    let brilho = ['opaco','cintilante','ofuscado','reluzente','destemido']
    let espuma = ['sustentada','fina','densa','viscosa','elástica']
    let aroma = ['cravo','jasmin','laranja','banana','biscoito de café']
    let boca = ['salgada','agridoce','picante','refrescante','umami']
    let corpo = ['viscoso','sedutor','magra','esguio','charmoso']
    let rescencia = ['seca','carbonada','na medida','proporcional','limpa']
    let amargor = ['intenso','jovem','fraco','destemido','caliente']
    let aftertaste = ['prolongado','inexistente','infinito','ousado','desagradável']
    
    let reviewCompleto = `Uma cerveja de cor ${cor[gen()]}, com brilho ${brilho[gen()]}, espuma ${espuma[gen()]}. O Aroma ${aroma[gen()]} combina perfeitamente com a boca ${boca[gen()]}, corpo ${corpo[gen()]} e rescência ${rescencia[gen()]}. Percebemos uma cerveja de amargor ${amargor[gen()]} e um aftertaste ${aftertaste[gen()]}.`

    return reviewCompleto

    }
}

module.exports = generatorService