require('dotenv').config()
const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const os = require("os");
// const app = express();
const webRoutes = require('./src/router/webRoutes');
const cookieParser = require('cookie-parser');

const app = express();
 
app.set('view engine', 'ejs');
app.set('views', 'src/views');

app.use(bodyParser.urlencoded({ extended: false }));
app.use("/public",express.static(__dirname + '/src/html'));
// app.use("/public",express.static(__dirname + '/public'));

app.use(cookieParser());

app.use(bodyParser.json()); // application/json


app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

app.use('/', webRoutes);

app.listen(process.env.PORT, function () {
    let msgServerUp = 
    `Server running HTTP on Port ${process.env.PORT} | Server: ${os.hostname()} | NodeEnv: ${process.env.NODE_ENV}`;
    console.log(msgServerUp);
});